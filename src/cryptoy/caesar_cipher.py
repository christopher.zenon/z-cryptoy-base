from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement de César


def encrypt(msg: str, shift: int) -> str:
    return unicodes_to_str([(x+shift)%110000 for x in str_to_unicodes(msg)])
    pass


def decrypt(msg: str, shift: int) -> str:
    return encrypt(msg, -shift)
    pass

def attack() -> tuple[str, int]:
    s = "恱恪恸急恪恳恳恪恲恮恸急恦恹恹恦恶恺恪恷恴恳恸急恵恦恷急恱恪急恳恴恷恩怱急恲恮恳恪恿急恱恦急恿恴恳恪"
    targt = "ennemis"

    for shift in range(110000):
        decrypted_msg = decrypt(s, shift)
        if targt in decrypted_msg:
            print("MSG : ",decrypted_msg)
            print("SHIFT : ", shift)
            return decrypted_msg, shift

    raise RuntimeError("Failed to attack")

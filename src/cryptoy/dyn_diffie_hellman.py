import random
import sys
import hashlib

from cryptoy.utils import (
    pow_mod,
)

sys.setrecursionlimit(5000)  # Required for pow_mod for large exponents


def keygen(prime_number: int, generator: int) -> dict[str, int]:
    # Générateur de clé de Diffie-Hellman
    # 1. Tire aléatoirement un nombre secret private_key entre 2 et prime_number - 1 inclus avec random.randint(min, max)
    # 2. Calcule la clé publique public_key = generator ** private_key % prime_number en utilisant pow_mod
    # 3. Renvoie le dictionnaire {"public_key": public_key, "private_key": private_key}
    private_key = random.randint(2, prime_number-1)
    public_key = pow_mod(generator, private_key, prime_number)
    return {"public_key": public_key, "private_key": private_key}


def compute_shared_secret_key(public: int, private: int, prime_number: int) -> int:
    # Calcul de la clé secrète partagée à partir de la clé publique de l'autre participant et de ma clé privée
    # Utilise pow_mod pour effectuer le calcul
    return pow_mod(public, private, prime_number)


class History:
    def __init__(self):
        self.histo = []
        self.curr_secret = []

    def new_secret(self, previous_hash=None):
        # spawn
        secret = {
            'index': len(self.histo) + 1,
            'timestamp': time(),
            'transactions': self.curr_secret,
            'previous_hash': previous_hash or self.hash(self.histo[-1]),
        }

        # Réinitialise le curs
        self.curr_secret = []

        # add the shift
        self.histo.append(secret)
        return secret

    def new_dh(self, sender, secret_hash):
        # spawnner
        self.curr_secret.append({
            'sender': sender,
            'secret_hash': secret_hash,
        })

    @staticmethod
    def hash(secret):
        bsecret = bytes(secret, 'utf-8')
        h = hashlib.new('sha256')
        h.update(bsecret)

        return h.hexdigest()

       # return lambda secret, N  : urlsafe_b64encode(((hashu(secret)+2**64*hash(secret+"2"))%(2**(N*8))).to_bytes(N,"big")).decode("utf8").rstrip("=")
        #pass

    @property
    def last_secret(self):
        # return
        pass

# Généeation de tempo aléatoire -- brancher sur la diffie
def generate_random_tempo():
    return random.randint(10, 500)

# Gestionnaire Shift
def derive_public_key(tempo, previous_key):
    ####
    return pow_mod(previous_key, tempo, prime_number)
